import java.util.*;

public class Employee {

    static String name;
    Adress adress;

        /*
        int[] arr = new int[unikalusMiestai.size()];
        int index = 0;
        for(String i : unikalusMiestai){
            arr[index++] = i;
        }
        */

    public static class Adress {

        public String city;

        public String street;

        public String number;

        public Adress(String name, String city, String street, String number) {
            Employee.name = name;
            this.city = city;
            this.street = street;
            this.number = number;
        }

        public static String[] addElement(String[] a, String e) {
            a  = Arrays.copyOf(a, a.length + 1);
            a[a.length - 1] = e;
            return a;
        }

        public static void skaiciuotiSkirtingusMiestus(Employee.Adress[] employees ) {
            String[] miestai = new String[1];
            for (int i = 0; i < employees.length; i++) {
                miestai = addElement(miestai, employees[i].city);
            }
            //ctrl+q kad atidarytu dokumentacija paspaudus ant norimo metodo
            Set<String> unikalusMiestai = new LinkedHashSet<>();
            for (String x : miestai) {
                if(x != null)
                unikalusMiestai.add(x);
            }
            System.out.println("Skirtingi miestai: " + unikalusMiestai.size());
/*
            HashSet hs = new HashSet();
            // add elements to the hash set
            hs.add(miestai);
            System.out.println(hs.size());
*/
        }
    }
}
