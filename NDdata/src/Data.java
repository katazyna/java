import java.math.BigDecimal;
import java.time.LocalDate;

public class Data {

    private LocalDate data;
    private String vardas;
    private BigDecimal suma;


    public Data(LocalDate data, String vardas, BigDecimal suma) {
        this.data = data;
        this.vardas = vardas;
        this.suma = suma;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        vardas = vardas;
    }

    public BigDecimal getSuma() {
        return suma;
    }

    public void setSuma(BigDecimal suma) {
        this.suma = suma;
    }


    @Override
    public String toString() {
        return "Data{" +
                "data=" + data +
                ", Vardas='" + vardas + '\'' +
                ", suma=" + suma +
                '}';
    }
}
