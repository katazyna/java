import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Data> sarasas = new ArrayList<>();

        sarasas.add(new Data(LocalDate.of(2016, 1, 12), "Auris", new BigDecimal(75)));
        sarasas.add(new Data(LocalDate.of(2016, 2, 12), "Jurgis", new BigDecimal(85)));
        sarasas.add(new Data(LocalDate.of(2016, 3, 12), "Petras", new BigDecimal(175)));
        sarasas.add(new Data(LocalDate.of(2016, 4, 12), "Timis", new BigDecimal(50)));
        sarasas.add(new Data(LocalDate.of(2016, 5, 12), "Joris", new BigDecimal(100)));
        sarasas.add(new Data(LocalDate.of(2016, 6, 12), "Auris", new BigDecimal(125)));
        sarasas.add(new Data(LocalDate.of(2016, 7, 12), "Stasys", new BigDecimal(175)));
        sarasas.add(new Data(LocalDate.of(2016, 8, 12), "Jurgis", new BigDecimal(55)));
        sarasas.add(new Data(LocalDate.of(2016, 9, 12), "Ona", new BigDecimal(75)));
        sarasas.add(new Data(LocalDate.of(2016, 10, 12), "Ana", new BigDecimal(120)));
        sarasas.add(new Data(LocalDate.of(2016, 11, 12), "Jonas", new BigDecimal(115)));
        sarasas.add(new Data(LocalDate.of(2016, 12, 12), "Timis", new BigDecimal(100)));

        for (Data d : sarasas){
            System.out.println(d);
        }

        Map<Integer, Map<Integer, BigDecimal>> reiksmes = sarasas.stream()
                .collect(Collectors.groupingBy(d->d.getData().getYear(), Collectors.toMap(d->d.getData().getMonth().getValue()<4?1:
                d.getData().getMonth().getValue()<7?2:
                d.getData().getMonth().getValue()<10?3:4, a->a.getSuma(),(a1,a2)->a1.add(a2)
                )));

        System.out.println(reiksmes);

    }
}
