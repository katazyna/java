import java.util.Arrays;
import java.util.*;
import java.util.Scanner;

public class Mokiniai {

    String vardas;
    String pavarde;
    int klase;
    int[] pazymiai = new int [5];
    double pazymiuVidurkis;



    //konstruktorius
    Mokiniai(String vardas, String pavarde, int klase, int [] pazymiai) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiai = pazymiai;
    }

    public void priskirtiPazymi(int pazymys) {
        pazymiai = addElement(pazymiai, pazymys);
    }
    public static int[] addElement(int[] a, int e) {
        a  = Arrays.copyOf(a, a.length + 1);
        a[a.length - 1] = e;
        return a;
    }

    /*
    public void pridetiPazymi(){
    public int[] pazymiai = new int[100];
    private int pazymiuKiekis = 0;
        this.pazymiai[pazymiuKiekis] = pazymys;
        this.pazymiuKiekis++;
    }
     */

    public void spausdintiPazymius() {
        for (int i = 0; i < pazymiai.length; i++)
            System.out.println(pazymiai[i]);
    }


    public double skaiciuotiVidurki() {
        int pazymiuSuma = 0;
        for(int i = 0; i < pazymiai.length; i++) {
            pazymiuSuma += pazymiai[i];
        }
        pazymiuVidurkis = pazymiuSuma * 1.0 / pazymiai.length;
        //System.out.println("Pazymiu vidurkis: " + pazymiuVidurkis);
        return pazymiuVidurkis;
    }

    public static void spausdintiKlasiuVidurkius(Mokiniai[] mokiniai) {
        int [] klases = new int [1];
        for(int i = 0; i < mokiniai.length; i++) {
            klases = addElement(klases, mokiniai[i].klase);
        }
        Set<Integer> unikaliosKlases = new LinkedHashSet<Integer>();
        for(int x : klases) { // ciklas per visa klases skaiciu masyva
            if (x != 0)  // 0, nes be jo iskarto meta 0 pirma nari
                unikaliosKlases.add(x); //pridedam i Seta skaicius (taip lieka tik  unikalus) IS PAVYZDZIO
        }

        int[] arr = new int[unikaliosKlases.size()]; // sukuriam nauja masyva tokio dydzio koks yra unikaliosKlases dydis
        int index = 0;
        for( Integer i : unikaliosKlases ) { // ciklas pereiti per visa unikaliosKlases Seta
            arr[index++] = i; // perrasom i paprasta masyva is Seto
        }

        Klases[] klasiuObjektai = new Klases[arr.length]; // sukuriam naujos sukurtos klases objektu masyva, tokio dydzio kaip arr.length
        for (int i = 0; i< arr.length; i++)
        {
            klasiuObjektai[i] = new Klases (klasesVidurkis(arr[i],mokiniai),arr[i]); // uzpildom klasiuobjektus su atitinkama Klase ir jos vidurkiu
            System.out.println("Klase - " +klasiuObjektai[i].klasesNumeris+ " sios klases vidurkis " + klasiuObjektai[i].vidurkis);
        }
        bubbleSortKlasesPagalVidurki(klasiuObjektai);
        System.out.println("---PO RIKIAVIMO ---");
        for (int i = 0; i < klasiuObjektai.length; i++)
        {
            System.out.println("Klase - " +klasiuObjektai[i].klasesNumeris+ " sios klases vidurkis " + klasiuObjektai[i].vidurkis);
        }

    }


    /*public static int mokiniuSkaiciusKlaseje(int klase, Mokiniai[] mokiniai) {
        int [] mokiniuSk = new int [12];
        for(int i = 0; i < mokiniai.length; i++) {
            mokiniuSk = addElement(mokiniuSk, mokiniai[i].klase);
        }
        Set<Integer> unikaliosKlases = new LinkedHashSet<Integer>();
        for(int x : mokiniuSk) { // ciklas per visa mokiniuSkaiciaus skaiciu masyva
            if (x != 0)  // 0, nes be jo iskarto meta 0 pirma nari
                unikaliosKlases.add(x); //pridedam i Seta skaicius (taip lieka tik  unikalus) IS PAVYZDZIO
        }
        return
    }*/

    public static void bubbleSortKlasesPagalVidurki(Klases[] mokiniai) {
        for (int i = 0; i < mokiniai.length - 1; i++) {
            for (int j = 0; j < mokiniai.length - 1; j++) {
                if (mokiniai[j].vidurkis < mokiniai[j + 1].vidurkis) {
                    Klases temp = mokiniai[j];
                    mokiniai[j] = mokiniai[j + 1];
                    mokiniai[j + 1] = temp;
                }
            }
        }
    }



    /*public static void countPairs(Mokiniai[] mokiniai) {
        int [] klases;
        for(int i = 0; i < mokiniai.length; i++) {
            for(int j = i+1; j <mokiniai.length; j++) {
                if((mokiniai[i].klase != mokiniai[j].klase) && (i != j)) {
                    vidurkiuKiekis++;
                    vidurkiuSuma += mokiniai[i].skaiciuotiVidurki();

                }

                return vidurkis =  vidurkiuSuma / vidurkiuKiekis;
            }
            System.out.println(mokiniai[i].klase + " Vidurkis yra: " + vidurkis);
        }


    }*/


    public static double klasesVidurkis(int klase, Mokiniai[] mokiniai) {
        double klasesVidurkis = 0;
        int mokiniuKiekisKlaseje = 0;
        for (int i = 0; i < mokiniai.length; i++) {
            if (mokiniai[i].klase == klase) {
                klasesVidurkis += mokiniai[i].skaiciuotiVidurki();
                mokiniuKiekisKlaseje++;
            }
        }
        return klasesVidurkis *1.0 /mokiniuKiekisKlaseje;
    }

    public static int mokiniuSkaicius(int klase, Mokiniai[] mokiniai) {
        int mokiniuKiekisKlaseje = 0;
        for(int i = 0; i < mokiniai.length; i++) {
            if(mokiniai[i].klase == klase) {
                mokiniuKiekisKlaseje++;
            }
        }
        return mokiniuKiekisKlaseje;
    }

    public static void spauMokSkKlaseje(Mokiniai[] mokiniai) {
        int[] klasiuSkaicius = new int[12];
        int klasiunum = 1;
        for (int i = 0; i < klasiuSkaicius.length; i++) {
            klasiuSkaicius[i] = klasiunum;
            klasiunum++;
            System.out.println(klasiuSkaicius[i] + " klaseje yra " + Mokiniai.mokiniuSkaicius(klasiuSkaicius[i], mokiniai) + " mokiniu");
        }
    }



    public static void konsolesIvestys (Mokiniai[] mokiniai,String input, String input1, int inputas) {
        for (int i = 0; i < mokiniai.length; i++)
        {
            if (input != null && input1 != null) {
                if (input.equals(mokiniai[i].vardas)) {
                    mokiniai[i].priskirtiPazymi(inputas);
                    System.out.println("Sekmingai ivestas pazymys: " + inputas);
                    System.out.println("Dabartiniai mokinio pazymiai: ");
                    mokiniai[i].spausdintiPazymius();
                    break;
                } else {
                    if (i == mokiniai.length-1) {
                        System.out.println("Mokinio su tokiu vardu nerasta ");
                        break;
                    }
                }
            }
            else {
                System.out.println("Butina ivesti varda ");
                break;
            }

        }
    }


    public static void bubbleSort(Mokiniai[] mokiniai) {
        for (int i = 0; i < mokiniai.length - 1; i++) {
            for (int j = 0; j < mokiniai.length - 1; j++) {
                if (mokiniai[j].skaiciuotiVidurki() > mokiniai[j + 1].skaiciuotiVidurki()) {
                    Mokiniai temp = mokiniai[j];
                    mokiniai[j] = mokiniai[j + 1];
                    mokiniai[j + 1] = temp;
                }
            }
        }
    }


    public static void spausdintiMokiniuDuomenis(Mokiniai[] mokiniai) {
        for(int i = 0; i < mokiniai.length; i++) {
            System.out.println(mokiniai[i].vardas + " " + mokiniai[i].pavarde + ", Vidurkis: " + mokiniai[i].skaiciuotiVidurki());
            //System.out.println(mokiniai[3].skaiciuotiVidurki());
        }
    }

    public static double surastiGeriausiaMokini(Mokiniai[] mokiniai) {
        double max = 0;
        for(int i = 0; i <mokiniai.length; i++) {
            if(mokiniai[i].skaiciuotiVidurki() > max) {
                max = mokiniai[i].skaiciuotiVidurki();
            }
        }
        return max;
    }

    public static void spausdintiGeriausiaMokini(Mokiniai[] mokiniai){
        for(int i = 0; i < mokiniai.length; i++) {
            if(surastiGeriausiaMokini(mokiniai) == mokiniai[i].skaiciuotiVidurki()) {
                System.out.println("Geriausias mokinys yra: " + mokiniai[i].vardas + " " + mokiniai[i].pavarde + " ir jo vidurkis yra: " + surastiGeriausiaMokini(mokiniai));
            }
        }
    }







}
