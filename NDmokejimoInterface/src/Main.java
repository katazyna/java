public class Main {
    public static void main(String[] args) {

        Client[] klientai = new Client[3];

        klientai[0] = new Client("Klientas 1", "LT1344");
        klientai[1] = new Client("Klientas 2", "LT3333");
        klientai[2] = new Client("Klientas 3", "LT1111");

        for(int i = 0; i < klientai.length; i++) {
            System.out.println(klientai[i].getAccountOwner() + " " + klientai[i].getBankAccount());

        }

        Employee[] darbuotojai = new Employee[3];

        darbuotojai[0] = new Employee("Darbuotojas 1", "LT5488");
        darbuotojai[1] = new Employee("Darbuotojas 2", "LT5489");
        darbuotojai[2] = new Employee("Darbuotojas 3", "LT5473");

        for(int i = 0; i < klientai.length; i++) {
            System.out.println(darbuotojai[i].getAccountOwner() + " " + darbuotojai[i].getBankAccount());

        }

    }
}
