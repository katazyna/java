import java.io.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
         try (
                 BufferedReader in = new BufferedReader(

                                 new FileReader("C:\\Users\\Katažyna\\IdeaProjects\\NDWikipedia\\src\\Text.txt")))
         {
             String[] masyvas = in.readLine().split("\\W+");

             Map<String, Long> count1 = Arrays.stream(masyvas).collect(Collectors.groupingBy(y->y.toLowerCase(), Collectors.counting()))
                     .entrySet().stream().sorted((g,h)->g.getValue().equals(h.getValue())?g.getKey().compareTo(h.getKey()):h.getValue().compareTo(g.getValue())).collect(Collectors.toMap(x -> x.getKey(), y -> y.getValue(),(a, b)->a, LinkedHashMap::new));

             for(String k: count1.keySet()){
                 System.out.println(k+" "+ count1.get(k));
             }

         } catch (FileNotFoundException e) {


         } catch (IOException e) {
             e.printStackTrace();
         }


         }


    }

