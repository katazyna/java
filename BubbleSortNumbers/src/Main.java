public class Main {

    public static void main(String[] args) {
        // surasti visus skaicius nuo 1 iki 1000

        int[] masyvas = {10, 5, 100, 200, 14, 15, 20, 3, 8, 500};

        int masyvoIlgis = masyvas.length - 1;
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < masyvoIlgis; j++){

                // Lyginam du skaicius kaimynus
                // Jei musu j skaicius yra didesnis uz j+1 tada:
                if(masyvas[j] > masyvas[j+1]) {
                    // Sukeiciame kintamuosius esancius salia vietomis
                    int temp = masyvas[j];
                    masyvas[j] = masyvas[j+1];
                    masyvas[j+1] = temp;
                }
            }

        }
        for (int i = 0; i < masyvas.length; i++)
        {
            System.out.println(masyvas[i]);
        }
        System.out.println("Baigiau darba");
    } // main funkcijos pabaiga
}

