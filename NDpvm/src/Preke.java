import java.math.BigDecimal;
import java.math.RoundingMode;

public class Preke {

    static int pvm = 21;
    private BigDecimal kaina;
    private BigDecimal pvmSuma;
    private String pavadinimas;
    private int kiekis;

    Preke(BigDecimal kaina, String pavadinimas){
        this.kaina = kaina;
        this.pavadinimas = pavadinimas;
        this.kiekis = 1;
        this.skaiciuotiPVMSuma(2);
    }

    //void nes nieko negrazinam, nera return
    public void setKiekis(int kiekis){
        this.kiekis = kiekis;
    }

    public int getKiekis(){
        return kiekis;
    }

    public void setPavadinimas(String pavadinimas){
        this.pavadinimas = pavadinimas;
    }

    public String getPavadinimas(){
        return pavadinimas;
    }

    public void setKaina(BigDecimal kaina){
        this.kaina = kaina;
    }

    public BigDecimal getKaina(){
        return kaina;
    }

    public void skaiciuotiPVMSuma(int tikslumas) {
        this.pvmSuma =  this.kaina.divide(new BigDecimal(100)).multiply(new BigDecimal(Preke.pvm)).setScale(tikslumas, RoundingMode.HALF_UP);
    }

    public void pridetiPreke(){
        kiekis++;
    }

    public double sumaSuPVM(){
        double test = this.kaina.add(this.pvmSuma).doubleValue();
        System.out.println(test);
        return test;
    }

    public void spausdinti(int tikslumas){
        System.out.println("Preke: " + this.pavadinimas);
        System.out.println("Kiekis: " + this.kiekis );
        System.out.println("Prekes PVM suma:" + this.pvmSuma);
        System.out.println("Prekes kaina be PVM: " + this.kaina);
        System.out.println("Prekes kaina su PVM: " + this.kaina.add(this.pvmSuma));
        System.out.println("Bendra kaina su PVM: " + this.kaina.add(this.pvmSuma).multiply(new BigDecimal(this.kiekis)));
        System.out.println("Bendra kaina be PVM: " + this.kaina.multiply(new BigDecimal(this.kiekis)));
    }

}
