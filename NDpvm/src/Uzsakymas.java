public class Uzsakymas {
    Preke[] prekes = new Preke[10];
    int prekiuKiekis = 0;

    public void pridetiPreke(Preke preke){
        this.prekes[prekiuKiekis] = preke;
        this.prekiuKiekis++;
    }

    public double uzsakymoSumaSuPVM(){
        double suma = 0;
        for(int i = 0; i < this.prekiuKiekis; i++){
            suma+= this.prekes[i].sumaSuPVM();
            System.out.println(suma);
        }
        return suma;
    }

}
