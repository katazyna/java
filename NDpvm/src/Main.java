import java.math.BigDecimal;

public class Main {

    public static void main(String[] args){

        Preke preke1 = new Preke(new BigDecimal("20.50"), "Gera preke");
        preke1.pridetiPreke();
        //preke1.spausdinti(2);
        Preke preke2 = new Preke(new BigDecimal("10.50"), "Bloga preke");


        //kuriame uzsakymo objekta
        Uzsakymas uzsakymas1 = new Uzsakymas();

        //uzsakymui priskiriame prekes
        uzsakymas1.pridetiPreke(preke1);
        uzsakymas1.pridetiPreke(preke2);

        System.out.println("Uzsakymo suma: " + round(uzsakymas1.uzsakymoSumaSuPVM(), 2));
    }
    public static double round(double value, int places){
        if(places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
