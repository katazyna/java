import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class D {

    public static void main(String[] args) {
        D d = new D();
        d.darbuotojoGavimas("Tadas");

        D d1 = new D();
        d1.algosPerskaiciavimas("Tadas", BigDecimal.valueOf(0.8));
    }


    public void algosPerskaiciavimas(String vardas, BigDecimal skaicius) {
        List<Darbuotojai> darbuotojai = new ArrayList<>();
        darbuotojai.add(new Darbuotojai("Jonas", BigDecimal.valueOf(1000)));
        darbuotojai.add(new Darbuotojai("Sigita", BigDecimal.valueOf(2500)));
        darbuotojai.add(new Darbuotojai("Zenonas", BigDecimal.valueOf(3000)));
        darbuotojai.add(new Darbuotojai("Tadas", BigDecimal.valueOf(600)));
        darbuotojai.add(new Darbuotojai("Ausra", BigDecimal.valueOf(1250)));

        List<Darbuotojai> isrinktasDarbuotojas = filtruoti(darbuotojai, vardas);

        isrinktasDarbuotojas = paskaiciuoti(isrinktasDarbuotojas, new Skaiciuoti() {
            @Override
            public BigDecimal countSalary(Darbuotojai d1) {
                //darome set, kad veliau galetume i List<Darbuotojai> ideti nauja atlyginima
                d1.setAtlyginimas(d1.getAtlyginimas().multiply(skaicius));
                return d1.getAtlyginimas();
            }
        });
        System.out.println("Nauja alga: " + isrinktasDarbuotojas);
    }


    public void darbuotojoGavimas(String vardas) {
        List<Darbuotojai> darbuotojai = new ArrayList<>();
        darbuotojai.add(new Darbuotojai("Jonas", BigDecimal.valueOf(1000)));
        darbuotojai.add(new Darbuotojai("Sigita", BigDecimal.valueOf(2500)));
        darbuotojai.add(new Darbuotojai("Zenonas", BigDecimal.valueOf(3000)));
        darbuotojai.add(new Darbuotojai("Tadas", BigDecimal.valueOf(600)));
        darbuotojai.add(new Darbuotojai("Ausra", BigDecimal.valueOf(1250)));

        List<Darbuotojai> isrinktasDarbuotojas = filtruoti(darbuotojai, vardas);

        isrinktasDarbuotojas = filtruoti2(isrinktasDarbuotojas, new Tikrinti() {
            @Override
            public boolean test(Darbuotojai d) {
                return d.getVardas().equalsIgnoreCase(vardas);
            }
        });
        System.out.println("Isrinktas darbuotojas: " + isrinktasDarbuotojas);
    }

    public List<Darbuotojai> filtruoti(List<Darbuotojai> sarasas, String vardas){
        List<Darbuotojai> rezultatas = new ArrayList<>();
        for(Darbuotojai d : sarasas){
            if(d.getVardas() != null &&
                d.getVardas().equalsIgnoreCase(vardas)){
                rezultatas.add(d);
            }
        }
        return rezultatas;
    }

    public List<Darbuotojai> filtruoti2(List<Darbuotojai> sarasas, Tikrinti tikrinti) {
        List<Darbuotojai> rezultatas = new ArrayList<>();
        for (Darbuotojai d : sarasas) {
            if (tikrinti.test(d)) {
                rezultatas.add(d);
            }
        }
        return rezultatas;
    }

    public List<Darbuotojai> paskaiciuoti(List<Darbuotojai> sarasas, Skaiciuoti skaiciuoti){
        List<Darbuotojai> rezultatas = new ArrayList<>();
        for (Darbuotojai d1 : sarasas) {
            skaiciuoti.countSalary(d1);
            rezultatas.add(d1);
        }
        return rezultatas;
    }
}



    interface Tikrinti{
        boolean test(Darbuotojai d);
    }

    interface Skaiciuoti{
        BigDecimal countSalary(Darbuotojai d1);
    }

    class Darbuotojai extends Object{

        private String vardas;
        private BigDecimal atlyginimas;

        public Darbuotojai(String vardas, BigDecimal atlyginimas) {
            this.vardas = vardas;
            this.atlyginimas = atlyginimas;
        }

        public String getVardas() {
            return vardas;
        }

        public void setVardas(String vardas) {
            this.vardas = vardas;
        }

        public BigDecimal getAtlyginimas() {
            return atlyginimas;
        }

        public void setAtlyginimas(BigDecimal atlyginimas) {
            this.atlyginimas = atlyginimas;
        }

        @Override
        public String toString() {
            return "Darbuotojai{" +
                    "vardas='" + vardas + '\'' +
                    ", atlyginimas=" + atlyginimas +
                    '}';
        }
    }

