public class Salary {

    Employee employee;
    double paidSalary;

    public Salary(Employee employee, double paidSalary) {
        this.employee = employee;
        this.paidSalary = paidSalary;
    }

    public String getNames(){
        return employee.getName();
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public double getPaidSalary() {
        return paidSalary;
    }

    public void setPaidSalary(double paidSalary) {
        this.paidSalary = paidSalary;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "employee=" + employee +
                ", paidSalary=" + paidSalary +
                '}';
    }
}
