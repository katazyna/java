import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<Salary> list = new ArrayList<>();
        list.add(new Salary(new Employee("Jonas"), 1000 ));
        list.add(new Salary(new Employee("Antanas"), 600 ));
        list.add(new Salary(new Employee("Antanas"), 800 ));
        list.add(new Salary(new Employee("Rimas"), 500 ));
        list.add(new Salary(new Employee("Jonas"), 1200 ));
        list.add(new Salary(new Employee("Tadas"), 700 ));
        list.add(new Salary(new Employee("Kazys"), 600 ));
/*
        System.out.println("Filtras---");
        list.stream().filter(e -> e.getPaidSalary() > 700). forEach(System.out::println);
*/

        //list.stream().collect(Collectors.groupingBy(e-> e.getEmployee().getName(), Collectors.reducing(0.0, x->x.getSalary(), (a, b) -> a+b))
        Map<String, Double> alguSuma = list.stream()
                .collect(Collectors.groupingBy(e-> e.employee.getName(), Collectors
                        .reducing(0.0, Salary::getPaidSalary, Double::sum)));
        /*
                .entrySet()
                .stream()
                .sorted(a, b) ->
        Double.compare(b.getValue(), a.getValue())
        )
        .forEach(x->
                System.out.println(x.getKey() + "ismoketa" + getValue())
        );
*/
        System.out.println("Darbuotoju algu sumos: " + alguSuma);


        Map<String, Long> skaiciavimas = list.stream().collect(Collectors.groupingBy(e-> e.employee.getName(), Collectors.counting()));
        System.out.println("Kiek kartu vyko ismokejimai: " + skaiciavimas);



/*
        List<Salary> sorted=list.stream().sorted((x, y) -> Double.compare(y.paidSalary, x.paidSalary)).collect(Collectors.toList());
        for(Salary x:sorted){
            System.out.println(x);
        }
*/
/*
        System.out.println("visu darbuotoju isrinkimas------");
        Set<String> s1 = list.stream()
                .map(Employee::getName)
                .collect(Collectors.toSet());
        System.out.println(s1);
*/




/*
        System.out.println("Bandymas-----");
        Map<String, Double> s2 = list.stream()
            .collect(Collectors.groupingBy(
              Salary.employee::getName,
              Collectors.reducing(0.0, Salary::getPaidSalary, Double::sum)));
        System.out.println("Algos: " + s2);
*/
//stream entry set, sorted, ir tada ka nori sorting map::entry getvalue.

    }
}
