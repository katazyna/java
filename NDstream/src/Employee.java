public class Employee implements Comparable<Employee> {

    private String name;

   // private double paidSalary;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Employee e){
        return name.compareTo(e.getName());
    }
/*
    public int compare(Employee e1, Employee e2){
        return e1.paidSalary.compareTo(e2.getPaidSalary());
    }
*/
    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                '}';
    }
}
