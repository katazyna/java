
public class Mokinys extends Zmogus {

    int klase;
    int[] pazymiai = new int[100];

    Mokinys(String vardas, String pavarde, int klase) {

        //kreipimasis i parent klases konstruktoriu
        super(vardas, pavarde);

        //priskiriama mokinio klase
        this.klase = klase;
}

    Mokinys(String vardas, String pavarde, int klase, int[] pazymiai) {

        //kreipimasis i parent klases konstruktoriu
        super(vardas, pavarde);

        //priskiriama mokinio klase;
        this.klase = klase;
        this.pazymiai = pazymiai;
    }
}
