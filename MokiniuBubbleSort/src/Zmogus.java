public class Zmogus {

    String vardas;
    String pavarde;
    int metai = 0;


    //konstruktorius
    Zmogus(String vardas, String pavarde) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        System.out.println("Sukuriau nauja zmogu");
    }

    public String pilnasVardas() {
        return this.vardas + " " + this.pavarde;
    }

    public boolean arPilnametis() {
        if(this.metai > 18) {
            return true;
        }else {
            return false;
        }
    }
}
