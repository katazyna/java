public class Main {

    public static void main(String[] args) {

        Mokinys[] mokiniai = new Mokinys[10];
        int mokiniuKiekis = 4;


        mokiniai[0] = new Mokinys("Zenonas", "Vaigauskas", 6);
        mokiniai[1] = new Mokinys("Petras", "Vaigauskas", 8);
        mokiniai[2] = new Mokinys("Antanas", "Brazauskas", 1);
        mokiniai[3] = new Mokinys("Eimantas", "Kasperiunas", 6);

        for (int i = 0; i < mokiniai.length -1 ; i++)
        {
            System.out.println(mokiniai[i].vardas);
            for (int j = 0; j < mokiniuKiekis - 1; j++)
            {
                if ((mokiniai[j].pavarde.compareToIgnoreCase(mokiniai[j + 1].pavarde) > 0))
                {
                    Mokinys temp = mokiniai[j];
                    mokiniai[j] = mokiniai[j + 1];
                    mokiniai[j + 1] = temp;

                }

                //tikrinimas jeigu pavardes vienodos
                else if (mokiniai[j].pavarde == mokiniai[j + 1].pavarde)
                {
                    if ((mokiniai[j].vardas.compareToIgnoreCase(mokiniai[j + 1].vardas) > 0))
                    {
                        Mokinys temp = mokiniai[j];
                        mokiniai[j] = mokiniai[j + 1];
                        mokiniai[j + 1] = temp;
                    }
                }

            }
        }
    }
}

