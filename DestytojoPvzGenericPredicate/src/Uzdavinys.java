import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * HelloWorld
 * Created by valdas on 2018-08-13.
 */
public class Uzdavinys {
    public static void main(String[] args) {
        Konteineris<Staciakampis> konteineris = new Konteineris<>();
        konteineris.add(new Staciakampis(Spalva.Balta, "Kazys", 8.0, 7.0));
        konteineris.add(new Staciakampis(null, "Petras", 16.0, 7.0));
        konteineris.add(new Staciakampis(null, "Ona", 8.0, 9.0));

        konteineris.print();

        System.out.println("Teisiogiai:");
        for (Staciakampis s : konteineris.tiesiogiai()) {
            System.out.println(s.name);
        }

        System.out.println("Atvirskstinis:");
        Predicate<Staciakampis> p1 = new Predicate<Staciakampis>() {
            @Override
            public boolean test(Staciakampis staciakampis) {
                return staciakampis.color != null;
            }
        };
        if (!konteineris.atv(p1).iterator().hasNext()) {
            System.out.println("Nera duomenu");
        }

        for (Staciakampis s : konteineris.atv(p1)) {
            System.out.println(s.name);
        }
    }
}


class Konteineris<T extends Figura> {

    List<T> dezute = new ArrayList<>();

    void add(T figura) {
        dezute.add(figura);
    }

    void print() {
        for(T figura : dezute) {
            System.out.println(figura.name + " " +
                    figura.color + " " +
                    (figura.color != null ? figura.color.kodas : ""));
        }
    }

    Iterable<T> tiesiogiai() {
        return new Iterable<T>() {

            @Override
            public Iterator<T> iterator() {
                return dezute.iterator();
            }
        };
    }

    Iterable<T> atv(Predicate<T> predicate) {
        return new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new Iterator<T>() {
                    int index = -1;
                    boolean pirmas = true;

                    int naujasIndex(int nuo) {
                        for (int i = nuo; i >= 0; i--) {
                            if (predicate.test(dezute.get(i))) {
                                return i;
                            }
                        }
                        return -1;
                    }

                    void start() {
                        if (pirmas) {
                            index = naujasIndex(dezute.size() - 1);
                            pirmas = false;
                        }
                    }

                    @Override
                    public boolean hasNext() {
                        start();
                        return index >= 0;
                    }

                    @Override
                    public T next() {
                        start();
                        T e = dezute.get(index);
                        // gauname nauja index reiksme
                        index = naujasIndex(index - 1);
                        return e;
                    }
                };
            }
        };
    }
}

enum Spalva {
    Raudona(0xff0000), Juoda(0), Balta(0xffffff), Zalia(0x00ff00);

    int kodas;

    Spalva(int kodas) {
        this.kodas = kodas;
    }
}

abstract class Figura {
    Spalva color;
    String name;

    public Figura(Spalva color, String name) {
        this.color = color;
        this.name = name;
    }
}

class Staciakampis extends Figura {
    double a;
    double b;

    public Staciakampis(Spalva color, String name, double a, double b) {
        super(color, name);
        this.a = a;
        this.b = b;
    }
}
