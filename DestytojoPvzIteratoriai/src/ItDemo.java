import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * HelloWorld
 * Created by valdas on 15/10/2018.
 */
public class ItDemo {

    public static void main(String[] args) {

        //Sarasas sarasas = new Sarasas();
        ExSarasas sarasas = new ExSarasas();

        sarasas.add(new Elementas("Pirmas"));
        sarasas.add(new Elementas("Antras"));
        sarasas.add(new Elementas("Trecias"));
        sarasas.add(new Elementas("Ketvirtas"));
        sarasas.add(new Elementas("Penktas"));

//        Iterator<Elementas> it = sarasas.iterator();
//////        while (it.hasNext()) {
//////            Elementas e = it.next();
        for (Elementas e : sarasas) {
            System.out.println(e.getVardas());
        }

        System.out.println("O dabar atbulai:");
        for (Elementas e : sarasas.atbulai()) {
            System.out.println(e.getVardas());
        }

    }
}

class Elementas {
    String vardas;

    public Elementas(String vardas) {
        this.vardas = vardas;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }
}

class ExSarasas extends ArrayList<Elementas> {

    @Override
    public Iterator<Elementas> iterator() {
        return new Iterator<Elementas>() {
            int index = 1;

            @Override
            public boolean hasNext() {
                return index < ExSarasas.this.size();
            }

            @Override
            public Elementas next() {
                Elementas e = ExSarasas.this.get(index);
                index += 2;
                return e;
            }
        };
    }

    public Iterable<Elementas> atbulai() {
        return new Iterable<Elementas>() {

            @Override
            public Iterator<Elementas> iterator() {
                return new Iterator<Elementas>() {
                    int index = ExSarasas.this.size() - 1;
                    boolean a = true;

                    @Override
                    public boolean hasNext() {
                        return index >= 0;
                    }

                    @Override
                    public Elementas next() {
                        Elementas e = ExSarasas.this.get(index);
                        index--;
                        return e;
                    }
                };
            }
        };
    }
}

class Sarasas implements Iterable<Elementas> {

    List<Elementas> listas = new ArrayList<>();

    public void add(Elementas e) {
        listas.add(e);
    }

    @Override
    public Iterator<Elementas> iterator() {

        //class ManoIteratorius implements Iterator<Elementas> {
        Iterator<Elementas> it = new Iterator<Elementas>() {

            int index = 1;

            @Override
            public boolean hasNext() {
                return index < listas.size();
            }

            @Override
            public Elementas next() {
                Elementas e = listas.get(index);
                index += 2;
                return e;
            }
        };

        //return new ManoIteratorius();
        return it;
    }
}