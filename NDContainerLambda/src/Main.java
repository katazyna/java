public class Main {

    public static void main(String[] args) {

        //kad rename visus box, tai ant jo paspaust, ir refactor->rename
        Container<Elementas> box = new Container<>();

        box.add(new Elementas("Pirmas"));
        box.add(new Elementas("Antras"));
        box.add(new Elementas("Trecias"));
        box.add(new Elementas("Ketvirtas"));

        for(Elementas e : box.order((e1, e2)-> e2.getVardas().compareTo(e1.getVardas()))){
            System.out.println(e);
        }
    }
}
