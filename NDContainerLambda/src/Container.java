import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Container<E> {

    List<E> listas = new ArrayList<>();

    public void add(E e){
        listas.add(e);
    }

    //ctrl laikyti ir spausti ant funkcijos,tada ismeta aprasyma
    public Iterable<E> order(Comparator<E> comp){
        List<E> list = new ArrayList<>(listas);
        list.sort(comp);
        return list;
    }


}
