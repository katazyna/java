import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class D {

    public static void main(String[] args) {

        Sarasas sarasas = new Sarasas();

        sarasas.add(new Elementas("Pirmas"));
        sarasas.add(new Elementas("Antras"));
        sarasas.add(new Elementas("Trecias"));
        sarasas.add(new Elementas("Ketvirtas"));
        sarasas.add(new Elementas("Penktas"));

        System.out.println("Atbulai: ");
        for(Elementas e : sarasas.atgalPirmyn()){
            System.out.println(e.getVardas());
        }
    }
}


class Elementas {
    String vardas;

    public Elementas(String vardas) {
        this.vardas = vardas;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }
}

class Sarasas implements Iterable<Elementas>{

    List<Elementas> listas = new ArrayList<>();

    public void add(Elementas e){
        listas.add(e);
    }

    @Override
    public Iterator<Elementas> iterator(){
        Iterator<Elementas> it = new Iterator<Elementas>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Elementas next() {
                return null;
            }
        };
        return it;
    }

    public Iterable<Elementas> atgalPirmyn(){
        return new Iterable<Elementas>() {
            @Override
            public Iterator<Elementas> iterator() {

                return new Iterator<Elementas>() {

                    int index = listas.size() - 1;
                    boolean pirmyn = false;

                    @Override
                    public boolean hasNext() {
                        return pirmyn && index < listas.size() ||
                                !pirmyn && index >=0;
                    }

                    @Override
                    public Elementas next() {
                        Elementas e = listas.get(index);

                        if(!pirmyn && index == 0){
                            pirmyn = true;
                        }
                        if(pirmyn) {
                            index++;
                        }
                        else{
                            index--;
                        }
                        return e;
                    }
                };
            }
        };
    }
}
