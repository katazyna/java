import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.List;

public class Saugykla<E> implements Iterable<E> {

    private ArrayList<E> saugykla;

    public Saugykla(){
        saugykla = new ArrayList<>();
    }

    public ArrayList<E> getSaugykla() {
        return saugykla;
    }

    public void add(E item){
        this.saugykla.add(item);
    }

    @Override
    public Iterator<E> iterator(){
        return saugykla.iterator();
    }

    public Iterator<E> iteratorAtgal(){
        return new Atgal(saugykla);
    }

    public Iterator<E> iteratorPirmyn(){
        return new Pirmyn(saugykla);
    }

    public Iterable<E> iteratorFiltravimas(Predicate<E> filtras){
        return new Iterable<E>(){
            @Override
            public Iterator<E> iterator(){
                return new FiltrasIterator<E>(saugykla, filtras);
            }
        };
    }



    class Pirmyn implements Iterator<E>{

        ArrayList<E> list;
        int index;

        public Pirmyn(ArrayList<E> list){
            this.list = list;
            this.index = 0;
        }
        @Override
        public boolean hasNext() {
            return index <= list.size()-1;
        }

        @Override
        public E next() {
            E el = list.get(index);
            index++;
            return el;
        }
    }


    class Atgal implements Iterator<E>{

        ArrayList<E> list;
        int index;

        public Atgal(ArrayList<E> list){
            this.list = list;
            this.index = list.size()-1;
        }
            @Override
            public boolean hasNext() {
                return index >= 0;
            }

            @Override
            public E next() {
                E el = list.get(index);
                index--;
                return el;
            }
    }

}
