import java.util.Iterator;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {

       Saugykla<Elementas> sarasas = new Saugykla<>();

        sarasas.add(new Elementas("Pirmas"));
        sarasas.add(new Elementas("Antras"));
        sarasas.add(new Elementas("Trecias"));
        sarasas.add(new Elementas("Ketvirtas"));
        sarasas.add(new Elementas("Penktas"));
        sarasas.add(new Elementas("Sestas"));

        //System.out.println(Daugiau.daugiauUz(2));


        System.out.println("----Filtras---");

        for(Elementas e : sarasas.iteratorFiltravimas(new Predicate<Elementas>() {
            @Override
            public boolean test(Elementas e) {
                return e.getVardas().equals("Penktas");
            }
        })) {
            System.out.println(e);
        }


        System.out.println("-----Pirmyn-----");
        for(Iterator<Elementas> it = sarasas.iteratorPirmyn(); it.hasNext();){
            Elementas e = it.next();
            System.out.println(e.getVardas());
        }

        System.out.println("-----Atgal-----");
        for(Iterator<Elementas> it = sarasas.iteratorAtgal(); it.hasNext();){
            Elementas e = it.next();
            System.out.println(e.getVardas());
        }


    }
}

