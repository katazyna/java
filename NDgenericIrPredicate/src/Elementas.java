public class Elementas {

    private String vardas;

    public Elementas(String vardas) {
        this.vardas = vardas;
    }

    public String getVardas() {
        return vardas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }


    @Override
    public String toString() {
        return "Elementas{" +
                "vardas=" + vardas +
                '}';
    }
}
