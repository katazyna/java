import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FiltrasIterator<E> implements Iterator<E> {

    private ArrayList<E> list;
    private int index;
    private Predicate<E> filtras;

    public FiltrasIterator(ArrayList<E> list, Predicate<E> filtras) {
        this.list = list;
        this.index = -1;
        this.filtras = filtras;
    }

    private int filter(ArrayList<E> list, Predicate<E> filtras){
        if(filtras == null)
            return -1;
        for(int i = index+1; i < list.size(); ++i){
            if(filtras.test(list.get(i))) return i;
        }
        return -1;
    }

    @Override
    public boolean hasNext(){
        index = filter(list, filtras);

        return index > -1;
    }

    @Override
    public E next(){
        return list.get(index);
    }

}
