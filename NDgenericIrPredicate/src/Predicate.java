public interface Predicate<E> {
    boolean test(E el);
}
