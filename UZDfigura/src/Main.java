public class Main {

    public static void main(String[] args) {
        double norimasSk = 100;
        // PIRMA UZDUOTIS ------------------------------------------------------------------------------
        Apskritimas a1 = new Apskritimas( Apskritimas.surastiApskritimoSpinduli(norimasSk));
        Kvadratas k1 = new Kvadratas(Kvadratas.surastiKvadratoKrastine(norimasSk));
        Trikampis t1 = new Trikampis (Trikampis.surastiTrikampioKrastine(norimasSk));

        System.out.println( "--------Norint gauti visu figuru plota = " + norimasSk);

        System.out.println( "Figuru perimetrai turi buti lygus: ");
        System.out.println( "Apskritimo perimetras = " + a1.perimetras());
        System.out.println("Apskritimo spindulys = " + Apskritimas.surastiApskritimoSpinduli(norimasSk));

        System.out.println( "Kvadrato perimetras = " + k1.perimetras());
        System.out.println("Kvadrato krastine = " + Kvadratas.surastiKvadratoKrastine(norimasSk));

        System.out.println( "Trikampio perimetras = " + t1.perimetras());
        System.out.println("Trikampio krastine = " + Trikampis.surastiTrikampioKrastine(norimasSk));
        // ANTRA UZDUOTIS ------------------------------------------------------------------------------

        Apskritimas a2 = new Apskritimas(Apskritimas.surastiApskritimo(norimasSk));
        Kvadratas k2 = new Kvadratas(Kvadratas.surastiKvadrato(norimasSk));
        Trikampis t2 = new Trikampis(Trikampis.surastiTrikampio(norimasSk));

        System.out.println("--------Norint gauti visu figuru perimetra = " + norimasSk);

        System.out.println("Apskritimo plotas = " + a2.plotas());
        System.out.println("Apskritimo krastine = " + Apskritimas.surastiApskritimo(norimasSk));

        System.out.println("Kvadrato plotas = " + k2.plotas());
        System.out.println("Kvadrato krastine = " + Kvadratas.surastiKvadrato(norimasSk));

        System.out.println("Trikampio plotas = " + t2.plotas());
        System.out.println("Trikampio krastine = " + Trikampis.surastiTrikampio(norimasSk));



    }
}
