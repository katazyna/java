class Trikampis extends Figura {

    private double krastine;

    //konstruktorius
    Trikampis(double krastine) {
        this.krastine = krastine;
    }

    public double perimetras() {
        perimetras = this.krastine*3;
        return perimetras;
    }

    public double plotas() {
        plotas = (this.krastine * this.krastine * Math.sqrt(3))/4;
        return plotas;
    }

    public static double surastiTrikampioKrastine(double plotas) {
        double krastine = 0;
        return krastine = (2 * Math.sqrt(plotas)/ 1.31607);
    }

    public static double surastiTrikampio(double perimetras){
        double krastine = 0;
        return krastine = perimetras / 3;
    }
}
