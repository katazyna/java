public abstract class Figura {

    public double perimetras;
    public double plotas;


    public abstract double plotas();

    public abstract double perimetras();

}
