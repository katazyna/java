class Apskritimas extends Figura {

    private double spindulys;
    private double skersmuo;
    private static double pi = 3.14;

    //konstruktorius
    Apskritimas( double spindulys) {
        this.skersmuo = spindulys * 2;
        this.spindulys = spindulys;
    }

    public double perimetras(){
        perimetras = this.skersmuo * Apskritimas.pi;
        return perimetras;
    }

    public double plotas(){
        plotas = this.spindulys * this.spindulys * Apskritimas.pi;
        return plotas;
    }

    public static double surastiApskritimoSpinduli(double plotas) {
        double spindulys = 0;
        return spindulys = Math.sqrt(plotas/3.14);
    }

    public static double surastiApskritimo(double perimetras){
        double spindulys = 0;
        return spindulys = perimetras / 3.14;
    }


}
