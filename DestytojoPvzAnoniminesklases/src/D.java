import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * HelloWorld
 * Created by valdas on 10/10/2018.
 */
public class D {

    public static void main(String[] args) {
        D d = new D();
        d.start();

        // arba paprasciau:
        // new D().start();
    }


    public void start() {
        List<Emp> darbuotojai = new ArrayList<>();
        darbuotojai.add(new Emp("Ona", "Kasa", BigDecimal.valueOf(1000.0)));
        darbuotojai.add(new Emp("Jonas", "Kasa", BigDecimal.valueOf(600.0)));
        darbuotojai.add(new Emp("Birute", "Sandelis", BigDecimal.valueOf(1600.0)));
        darbuotojai.add(new Emp("Jonas", "Administracija", BigDecimal.valueOf(2600.0)));
        //TODO pabandyti ir su null irasu:
        darbuotojai.add(new Emp("Ecka", null, null));
        print(darbuotojai, "Pradiniai:");

        // 1. paprasciausias budas filtruoti pagal skyriu
        List<Emp> kasininkai = filtruoti(darbuotojai, "kasa");
        print(kasininkai, "Kasininkai 1:");

        // 2. Universalus filtravimas
        // 2.1. Pagal skyrius "kasa" su lokale klase (PagalKasa)
        class PagalKasa implements Tikrinti {

            @Override
            public boolean test(Emp e) {
                return e.getDepartment().equalsIgnoreCase("kasa");
            }
        }
        Tikrinti pagalKasa = new PagalKasa();
        kasininkai = filtruoti2(darbuotojai, pagalKasa);
        print(kasininkai, "Kasininkai 2.1:");

        // 2.2. Pagal skyrius "kasa" su anonimine klase ir
        // anonimines klases objektu tarpinimiame kintamajame (pagalKasa2)
        Tikrinti pagalKasa2 = new Tikrinti() {
            @Override
            public boolean test(Emp e) {
                return e.getDepartment().equalsIgnoreCase("kasa");
            }
        };
        kasininkai = filtruoti2(darbuotojai, pagalKasa2);
        print(kasininkai, "Kasininkai 2.2:");

        // 2.3. Pagal skyrius "kasa" su anonimine klase be tarpinio kintamojo
        kasininkai = filtruoti2(darbuotojai, new Tikrinti() {
            @Override
            public boolean test(Emp e) {
                return e.getDepartment().equalsIgnoreCase("kasa");
            }
        });
        print(kasininkai, "Kasininkai 2.3:");

        // 2.4. Pagal skyrius "kasa" su dideliu atlyginimu
        kasininkai = filtruoti2(darbuotojai, new Tikrinti() {

            @Override
            public boolean test(Emp e) {
                return e.getDepartment().equalsIgnoreCase("kasa") &&
                        e.getSalary().compareTo(BigDecimal.valueOf(1000.0)) >= 0;
            }
        });
        print(kasininkai, "Kasininkai 2.4:");


        //  a.compareTo(b) ? -x 0 +x
        //   a  >  b    -> +x
        //   a  ==  b   ->  0
        //   a  <  b    -> -x


    }

    /**
     * Spausdina sarasa su antraste
     * @param sarasas spausdinamas sarasas
     * @param antraste antraste
     */
    public void print(List<Emp> sarasas, String antraste) {
        System.out.println(antraste);
        for (Emp e : sarasas) {
            System.out.println(e);
        }
    }


    /**
     * Paprastas filtravimas pagal skyriu
     * @param sarasas
     * @param skyrius
     * @return
     */
    public List<Emp> filtruoti(List<Emp> sarasas, String skyrius) {
        List<Emp> rezultatas = new ArrayList<>();
        for (Emp e : sarasas) {
            if (e.getDepartment() != null &&
                    e.getDepartment().equalsIgnoreCase(skyrius)) {
                rezultatas.add(e);
            }
        }
        return rezultatas;
    }

    public List<Emp> filtruoti2(List<Emp> sarasas, Tikrinti tikrinti) {
        List<Emp> rezultatas = new ArrayList<>();
        for (Emp e : sarasas) {
            if(e.getDepartment() != null) {
                if (tikrinti.test(e)) {
                    if(e != null) {
                        rezultatas.add(e);
                    }
                }
            }
        }
        return rezultatas;
    }

}

interface Tikrinti {

    boolean test(Emp e);

}

class Emp extends Object {
    private String name;
    private String department;
    private BigDecimal salary;

    public Emp(String name, String department, BigDecimal salary) {
        this.name = name;
        this.department = department;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "name='" + name + '\'' +
                ", department='" + department + '\'' +
                ", salary=" + salary +
                '}';
    }
}
