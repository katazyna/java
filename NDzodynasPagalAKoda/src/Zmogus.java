
public class Zmogus {

    private String vardas;
    private String pavarde;

    Zmogus(String vardas, String pavarde){
        this.vardas = vardas;
        this.pavarde = pavarde;
    }

    public void setVardas(String vardas){
        this.vardas = vardas;
    }

    public String getVardas(){
        return vardas;
    }

    public void setPavarde(String pavarde){
        this.pavarde = pavarde;
    }

    public String getPavarde(){
        return pavarde;
    }

    //alt+insert->toString (paraso visa sita metoda,kaip spausdina Maine
    @Override
    public String toString() {
        return "Zmogus{" +
                "vardas='" + vardas + '\'' +
                ", pavarde='" + pavarde + '\'' +
                '}';
    }
}
