import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        Map<String, List<Zmogus>> sarasas = new TreeMap<>();

        /*
        List<Zmogus> people = new ArrayList<>();
        people.add(new Zmogus("Ada", "Adaitiene"));
        people.add(new Zmogus("Simas", "Simaitis"));
        sarasas.put(111, people);
        */
        sarasas.computeIfAbsent("11111111111", k -> new ArrayList()).add(new Zmogus("Ada", "Jonaitiene"));
        sarasas.computeIfAbsent("11111111111", k -> new ArrayList()).add(new Zmogus("Juozas", "Juozaitis"));
        sarasas.computeIfAbsent("49512305472", k -> new ArrayList()).add(new Zmogus("Tadas", "Tadaitis"));
        sarasas.computeIfAbsent("49623548265", k -> new ArrayList()).add(new Zmogus("Ausra", "Jonaitiene"));
        sarasas.computeIfAbsent("42518462547", k -> new ArrayList()).add(new Zmogus("Sigita", "Sigitaite"));

        /*
        sarasas.put(495, new Zmogus("Jonas", "Jonaitis"));
        sarasas.put(425, new Zmogus("Tadas",  "Tadaitis"));
        sarasas.put(265, new Zmogus("Ausra",  "Jonaitiene"));
        sarasas.put(236, new Zmogus("Sigita", "Tadaitiene"));
        */

        for(String m:sarasas.keySet()){
            System.out.println(m+ " " + sarasas.get(m));
        }

    }
}
